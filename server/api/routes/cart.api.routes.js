'use strict';

// Load the module dependencies
const cartCtrl = require('../controllers/cart.api.controller');

// Define the routes module' method
module.exports = function(app) {
  // ===========================Set up the 'cart' routes ==============================
  app.route('/cart')
      .get(cartCtrl.getCart)
      .post(cartCtrl.cartSave);

};