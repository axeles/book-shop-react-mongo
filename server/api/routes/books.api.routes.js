'use strict';

// Load the module dependencies
const booksCtrl = require('../controllers/books.api.controller');

// Define the routes module' method
module.exports = function(app) {
  // ===========================Set up the 'books' routes ==============================
  app.route('/books')
      .get(booksCtrl.booksList)
      .post(booksCtrl.booksCreate);

  app.route('/book/:bookId')
      .get(booksCtrl.booksReadOne)
      .delete(booksCtrl.booksDeleteOne)
      .put(booksCtrl.booksUpdateOne);

  app.get('/images', booksCtrl.getImagesList);

  // app.param('bookId', booksCtrl.booksReadOne);
};