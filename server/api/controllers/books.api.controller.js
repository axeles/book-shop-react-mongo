'use strict';

const fs = require('fs');
const path = require('path');
const Book  = require('mongoose').model('Book');
const utils = require('../../utils/index');

// ===== Create a new controller method that list of all books ==========
exports.booksList = function (req, res) {
  Book.find().exec(function(err, books) {
    if (err) {
      utils.sendJsonResponse(res, 400, err);
    } else {
      utils.sendJsonResponse(res, 201, books);
    }
  });
};

// ===== Create a new controller method that that creates new book ==========
module.exports.booksCreate = function (req, res) {
  const books = req.body;
  Book.create(books, function(err, book) {
    if (err) {
      utils.sendJsonResponse(res, 400, err);
    } else {
      utils.sendJsonResponse(res, 201, book);
    }
  });
};

// ===== Create a new controller method that returns the book with given bookID ======
exports.booksReadOne = function (req, res) {

  if (!req.params || !req.params.bookId) {
    utils.sendJsonResponse(res, 404, {"message": "No bookId in request"});
  }

  Book.findById(req.params.bookId)
    .exec(
      function(err, book) {
        if (!book) {
          utils.sendJsonResponse(res, 404, {
            "message": "book with such id not found"
          });
          return;
        } else if (err) {
          utils.sendJsonResponse(res, 404, err);
          return;
        }
        utils.sendJsonResponse(res, 200, book);
      }
    );
};

// ===== Create a new controller method that updates the book with given bookID ======
exports.booksUpdateOne = function (req, res) {
  if (!req.params.bookId) {
    utils.sendJsonResponse(res, 404, {
      "message": "Not found, bookId is required"
    });
    return;
  }
  Book.findById(req.params.bookId)
    .exec(
      function(err, book) {
        if (!book) {
          utils.sendJsonResponse(res, 404, {
            "message": "book not found"
          });
        } else if (err) {
          utils.sendJsonResponse(res, 400, err);
        } else {
          book.title = req.body.title;
          book.description = req.body.description;
          book.image = req.body.image;
          book.price = req.body.price;

          book.save(function (err, updatedBook) {
            if (err) {
              utils.sendJsonResponse(res, 404, err);
            } else {
              utils.sendJsonResponse(res, 200, updatedBook);
            }
          });
        }
      }
    );
};

// ===== Create a new controller method that deletes the book with given bookID ======
exports.booksDeleteOne = function (req, res) {
  if (!req.params || !req.params.bookId) {
    utils.sendJsonResponse(res, 404, {"message": "No bookId in request"});
  }

  let bookId = req.params.bookId;

  if (bookId) {
    Book.findByIdAndRemove(bookId)
      .exec( function(err, book) {
        if (err) {
          utils.sendJsonResponse(res, 404, err);
          return;
        }
        utils.sendJsonResponse(res, 204, book);
      });

  } else {
    utils.sendJsonResponse(res, 404, {"message": "No bookId"});
  }
};

// ===== Create a new controller method that fetch images from the file system ==========
module.exports.getImagesList = function (req, res) {
  const imgFolder = path.join(__dirname,  '../../../public/img');
  console.error.bind(console, '+++ imgFolder', imgFolder);
  fs.readdir(imgFolder, function (err, files) {
    if(err){
      console.error(err);
    }
    const images = [];
    files.forEach(function (file) {
      images.push({ name: file });
    });
    utils.sendJsonResponse(res, 201, images);
  });
};

