'use strict';

const utils = require('../../utils/index');

// ===== Create a new controller method that saves current cart ==========
module.exports.cartSave = function (req, res) {
  const cart = req.body;
  req.session.cart = cart;
  req.session.save(function (err) {
    if (err) {
      utils.sendJsonResponse(res, 400, err);
    } else {
      utils.sendJsonResponse(res, 201, cart);
    }
  });

};
// ===== Create a new controller method that saves current cart ==========
module.exports.getCart = function (req, res) {
    if (typeof req.session.cart !== 'undefined') {
      utils.sendJsonResponse(res, 201, req.session.cart);
    }
};

