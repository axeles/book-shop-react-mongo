"use strict";
const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
  title: String,
  description: String,
  image: String,
  price: Number
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;