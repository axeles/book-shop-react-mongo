// Invoke 'strict' JavaScript mode
'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const configPath = './env/' + NODE_ENV;
// Load the correct configuration file according to the 'NODE_ENV' variable
module.exports = require(configPath);