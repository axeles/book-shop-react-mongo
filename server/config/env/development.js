// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'development' environment configuration object
module.exports = {
	db: 'mongodb://localhost/book_shop',
	sessionSecret: 'developmentSessionSecret',
	proxyHost: 'http://localhost',
	proxyPort: 3001,
  apiOptions: {
	  server: 'http://localhost:3000'
  }
};