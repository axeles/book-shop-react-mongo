'use strict';

// Set the 'production' environment configuration object
module.exports = {
	db: 'mongodb://localhost:27017/book_shop',
	sessionSecret: 'productionSessionSecret',
  apiOptions: {
    server: 'http://localhost:3000'
  }
};