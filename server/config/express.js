'use strict';

const express      = require('express');
const path         = require('path');
const favicon      = require('serve-favicon');
const logger       = require('morgan');
const compress     = require('compression');
const config       = require('./config');
// const ejs          = require('ejs');
const sassMiddleware = require('node-sass-middleware');
const httpProxy = require('http-proxy');
const requestHandler = require('../requestHandler');

// Define the Express configuration method
module.exports = function() {

  // Create a new Express application instance
  const app = express();

  // Use the 'NDOE_ENV' variable to activate the 'morgan' logger or 'compress' middleware
  if (process.env.NODE_ENV === 'development') {
    app.use(logger('dev'));
  } else if (process.env.NODE_ENV === 'production') {
    app.use(compress());
  }

  app.use(favicon(path.join(__dirname, '../../public', 'favicon.ico')));
  app.use(sassMiddleware({
    src: path.join(__dirname, '../views/styles'),
    dest: path.join(__dirname, '../../public/css'),
    prefix: '/css',
    debug: true,
    outputStyle: 'compressed',
    indentedSyntax: true,
    sourceMap: true
  }));

  // Configure static file serving
  app.use(express.static(path.join(__dirname, '../../public')));
  app.use(express.static(path.join(__dirname, '../../node_modules')));

  // PROXY SERVER FOR API
  const apiProxy = httpProxy.createProxyServer({
    target: config.proxyHost + ':' + config.proxyPort
  });
  apiProxy.on('error', function (err, req, res) {
    res.end('Error occur'+ err);
  });
  app.use('/api', function (req, res) {
    apiProxy.web(req, res);
  });

  // view engine setup
  app.set('views', path.join(__dirname, '../views'));
  app.set('view engine', 'ejs');
  app.use(requestHandler);

  // app.get('*', function(req, res){
  //   res.sendFile(path.resolve(__dirname, '../../public', 'index.html'))
  // });

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render(err);
  });

// Return the Express application instance
  return app;
};
