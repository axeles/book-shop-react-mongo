'use strict';

const config   = require( './config' );
const mongoose = require( 'mongoose' );

// Define the Mongoose configuration method
module.exports = function() {

  mongoose.Promise = global.Promise;
  // Use Mongoose to connect to MongoDB
  let db = null;
  const promise = mongoose.connect(config.db, {useMongoClient: true});
  promise.then(function (connectedDB) {
    db = connectedDB;
  });

  mongoose.connection.on('connected', function () {
    console.log('Mongoose connected to ' + config.db);
  });

  mongoose.connection.on('error', function (err) {
    console.error.bind(console, '# Mongoose connection error: ' + err);
  });

  mongoose.connection.on('disconnected', function () {
    console.log('Mongoose disconnected');
  });

// emulates SIGINT event when the app finishing
  const readLine = require("readline");

  if (process.platform === "win32") {
    const rl = readLine.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    rl.on("SIGINT", function () {
      process.emit("SIGINT");
    });
  }

  const gracefulShutdown = function (msg, callback) {
    mongoose.connection.close(function () {
      console.log('Mongoose disconnected through ' + msg);
      console.log('Platform: ' + process.platform);
      callback();
    });
  };

// For nodemon restarts
  process.once('SIGUSR2', function () {
    gracefulShutdown('nodemon restart', function () {
      process.kill(process.pid, 'SIGUSR2');
    });
  });

// For app termination
  process.on('SIGINT', function () {
    gracefulShutdown('app termination', function () {
      process.exit(0);
    });
  });

// For Heroku app termination
  process.on('SIGTERM', function () {
    gracefulShutdown('Heroku app shutdown', function () {
      process.exit(0);
    });
  });

// Load models
  require('../api/models/book.model');

// Return the Mongoose connection instance
  return db;
};
