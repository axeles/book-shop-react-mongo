'use strict';

const express      = require('express');
const path         = require('path');
const favicon      = require('serve-favicon');
const logger       = require('morgan');
const compress     = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const session      = require('express-session');
const MongoStore   = require('connect-mongo')(session);
const config       = require('./config/config');
const sassMiddleware = require('node-sass-middleware');

// Create a new Express application instance
const app = express();

// Load the module dependencies
const mongoose = require('./config/mongoose');

// Create a new Mongoose connection instance
const db = mongoose();

// Use the 'NODE_ENV' variable to activate the 'morgan' logger or 'compress' middleware
if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
} else if (process.env.NODE_ENV === 'production') {
  app.use(compress());
}

app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Configure the 'session' middleware
app.use(session({
  saveUninitialized: false,
  resave: false,
  secret: config.sessionSecret,
  cookie: {maxAge: 1000 * 60 * 60 * 24 * 2}, // 2 days in milliseconds
  store: new MongoStore({
    mongooseConnection: db,
    url: config.db,
    collection: 'session',
    ttl: 2 * 24 * 60 * 60
  })
  // ttl 2 days * 24 hours * 60 minutes * 60 seconds
}));

app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, './views/styles'),
  dest: path.join(__dirname, '../public/css'),
  prefix: '/css',
  debug: true,
  outputStyle: 'compressed',
  indentedSyntax: true,
  sourceMap: true
}));

// Configure static file serving
app.use(express.static(path.join(__dirname, '../public')));
app.use(express.static(path.join(__dirname, '../node_modules')));

// Load the routing files
require('./api/routes/books.api.routes')(app);
require('./api/routes/cart.api.routes')(app);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render(err);
});

app.listen(config.proxyPort, function (err) {
  if(err) {
    return console.log(err);
  }
  console.log(`API server is listening on ${config.proxyHost}:${config.proxyPort}`);
});
