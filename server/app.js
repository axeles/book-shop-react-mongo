'use strict';

require('babel-core/register')({
  "presets":["es2015", "react", "stage-1"]
});

// Set the 'NODE_ENV' variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
if(process.env.NODE_ENV === 'development') {
  process.env.DEBUG = 'full-stack-react-redux:server';
}

// Load the module dependencies
// const mongoose = require('./config/mongoose');
const express  = require('./config/express');

// Create a new Mongoose connection instance
// const db = mongoose();

// Create a new Express application instance
const app = express();

// Use the module.exports property to expose our Express application instance for external usage
module.exports = app;