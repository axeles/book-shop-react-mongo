"use strict";

// const axios = require('axios');
// const React = require('react');
// const createStore = require('redux').createStore;
// const Provider = require('react-redux').Provider;
// const renderToString = require('react-dom/server').renderToString;
// const match = require('react-router').match;
// const RouterContext = require('react-router').RouterContext;

import axios from 'axios';
import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {renderToString} from 'react-dom/server';
import {match, RouterContext} from 'react-router';
import reducers from '../client/src/reducers/index';
import routes from '../client/src/routes';
// const reducers = require('../client/src/reducers/index');
// const routes = require('../client/src/routes');

const utils = require('./utils/index');

function handleRender(req, res) {
  axios.get('http://localhost:3001/books')
    .then(function (response) {
      // const myHtml = JSON.stringify(response.data);
      // res.render('index', {myHtml})

      // STEP-1 CREATE A REDUX STORE ON THE SERVER
      const store = createStore(reducers, {"books": {"books": response.data}});
      // STEP-2 GET INITIAL STATE FROM THE STORE
      const initialState = JSON.stringify(store.getState()).replace(/<\/script/g, '<\\/script').replace(/<!--/g, '<\\!--');
      // STEP-3 IMPLEMENT A REACT-ROUTER ON THE SERVER TO INTERCEPT CLIENT REQUESTS AND DEFINE WHAT TO DO WITH THEM
      const Routes = {
        routes,
        location: req.url
      };
      match(Routes, function (error, redirect, props) {
        if(error){
          utils.sendJsonResponse(res, 500, "Error fulfilling the request" + err);
        } else if(redirect) {
          utils.sendJsonResponse(res, 302, redirect.pathname + redirect.search);
        } else if(props) {
          const reactComponent = renderToString(
            <Provider store={store}>
              <RouterContext {...props}/>
            </Provider>
          );
          res.status(200).render('index', {reactComponent, initialState});
        } else {
          utils.sendJsonResponse(res, 404, "Page not found");
        }
      })
    })
    .catch(function (err) {
      console.log('#Initial server-side rendering error: ', err)
    })
}

module.exports = handleRender;