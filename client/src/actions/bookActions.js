"use strict";
import axios from 'axios';

// GET BOOKS
export function getBooks(){
  return function (dispatch) {
    axios.get('/api/books')
      .then(function (response) {
        dispatch({type: 'GET_BOOKS', payload: response.data})
      })
      .catch(function (err) {
        const payload = 'There was an error while getting books: ' + err;
        dispatch({type: 'GET_BOOKS_REJECTED', payload})
      })
  }
}

// POST BOOKS
export function postBooks(books){
  return function (dispatch) {
    axios.post('/api/books', books)
      .then(function(response){
        dispatch({type: 'POST_BOOKS', payload: response.data })
      })
      .catch(function(err){
        const payload = 'There was an error while posting new books: ' + err;
        dispatch({type: 'POST_BOOKS_REJECTED', payload })
      })
  }
}

// DELETE BOOKS
export function deleteBook(_id){
  const apiURL = '/api/book/' + _id;
  return function (dispatch) {
    axios.delete(apiURL)
      .then(function (response) {
        dispatch({type: 'DELETE_BOOK', payload: response.data})
      })
      .catch(function (err) {
        const payload = 'There was an error while deleting book: ' + err;
        dispatch({type: 'DELETE_BOOKS_REJECTED', payload})
      })
  }
}

// UPDATE BOOKS
export function updateBook(book){
  return {
    type: 'UPDATE_BOOK',
    payload: book
  }
}

// RESET BUTTON
export function resetButton(){
  return {
    type: 'RESET_BUTTON'
  }
}
