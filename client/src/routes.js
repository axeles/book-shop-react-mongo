'use strict';

import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

// APP COMPONENTS
import Main from './main';
import BooksList from './components/pages/booksList';
import BookForm from './components/pages/bookForm';
import Cart from './components/pages/cart';

const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={Main}>
      <IndexRoute component={BooksList} />
      <Route path="/admin" component={BookForm} />
      <Route path="/cart" component={Cart} />
    </Route>
  </Router>
);

export default routes;
