"use strict";

import {combineReducers} from 'redux';

// HERE IMPORT REDUCERS TO BE COMBINED
import {bookReducers} from './bookReducers';
import {cartReducers} from './cartReducers';

// HERE COMBINE THE REDUCERS
export default combineReducers({
  bookReducers,
  cartReducers
});
