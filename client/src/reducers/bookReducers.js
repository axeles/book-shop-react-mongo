
const initialState = {
  books: [],
  msg: null,
  style: null,
  validation: null
};
// STEP 3 - define reducers
export function bookReducers(state = initialState, action){

  switch (action.type) {
    case 'GET_BOOKS':
      return {...state, books: [...action.payload]};
    case 'POST_BOOKS':
      return {
        ...state,
        books: [...state.books, ...action.payload],
        msg: 'Saved! Click to continue',
        style: 'success',
        validation: 'success'
      };
    case 'POST_BOOKS_REJECTED':
      return {
        ...state,
        msg: 'Please, try again',
        style: 'danger',
        validation: 'error'
      };
    case 'RESET_BUTTON':
      return {
        ...state,
        msg: null,
        style: null,
        validation: null
      };
    case 'DELETE_BOOK':
      // Create a copy of current array of books
      const currentBooks = [...state.books];
      // Determine at which index in the book array is book to be deleted
      const indexOfBookToDelete = currentBooks.findIndex(function (book) {
        return book._id.toString() === action.payload;
      });
      return {
        ...state,
        books: [...currentBooks.slice(0, indexOfBookToDelete),
          ...currentBooks.slice(indexOfBookToDelete + 1)]
      };
    case 'UPDATE_BOOK':
      // Create a copy of current array of books
      const currBooks = [...state.books];
      // Determine at which index in the book array is book to be updated
      const indexOfBookToUpdate = currBooks.findIndex(function (book) {
        return book._id === action.payload._id;
      });
      const newBooks = [...currBooks[indexOfBookToUpdate], {
        title: action.payload.title,
        description: action.payload.description,
        price: action.payload.price
      }];
      return {
        ...state,
        books: [...newBooks]
      };
  }
  return state;
}
