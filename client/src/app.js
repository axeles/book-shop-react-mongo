'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

// REDUX MIDDLEWARE
import thunk from 'redux-thunk';
import logger  from 'redux-logger';
// IMPORT COMBINED REDUCERS
import reducers from './reducers/index';
import routes from './routes'

// WE WILL PASS INITIAL STATE FROM SERVER STORE
const initialState = window.INITIAL_STATE;

// CREATING STORE
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = applyMiddleware(thunk, logger);
const store = createStore(reducers, initialState, composeEnhancers(middleware));

const Routes = (
  <Provider store={store}>
    {routes}
  </Provider>
);
render(Routes, document.getElementById('app'));

