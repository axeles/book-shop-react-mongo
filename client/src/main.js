"use strict";
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Menu from './components/menu';
import Footer from './components/footer';
import { bindActionCreators } from 'redux';
import { getCart } from "./actions/cartActions";

class Main extends Component {
  componentDidMount() {
    this.props.getCart();
  }

  render() {
    return (
        <div className="main">
          <Menu cartItemNumber={this.props.totalQuantity}/>
          {this.props.children}
          <Footer />
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    totalQuantity: state.cartReducers.totalQuantity
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getCart
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Main);
