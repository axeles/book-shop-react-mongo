"use strict";

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';

import {
  MenuItem,
  InputGroup,
  DropdownButton,
  Image,
  Col,
  Row,
  Well,
  Panel,
  FormControl,
  FormGroup,
  ControlLabel,
  Button
} from 'react-bootstrap';
import { findDOMNode } from 'react-dom';
import { postBooks, deleteBook, getBooks, resetButton } from "../../actions/bookActions";

class BookForm extends Component {
  constructor() {
    super();
    this.state = {
      images: [],
      img: ''
    };
  }

  componentDidMount() {
    this.props.getBooks();
    axios.get('/api/images')
      .then(function (response) {
        this.setState({images: response.data});

      }.bind(this))
      .catch(function(err) {
        this.setState({images: [{name: 'error loading image files from the server'}], img: ''});
        console.log('Cannot read list of images:', err);
      }.bind(this));

  }

  handleSubmit() {
    const books = [
      {
        title: findDOMNode(this.refs.title).value,
        description: findDOMNode(this.refs.description).value,
        image: findDOMNode(this.refs.image).value,
        price: findDOMNode(this.refs.price).value
      }
    ];

    this.props.postBooks(books);
  };

  onDelete() {
    const bookTitle = findDOMNode(this.refs.delete).value;
    const bookId = this.props.books.filter(function (book) {
      return book.title === bookTitle;
    })[0]._id;
    this.props.deleteBook(bookId);
  };

  handleSelectImage(imgName) {
    this.setState({img: '/img/' + imgName});
  }

  resetForm() {
    // TODO: RESET THE BUTTON
    this.props.resetButton();
    findDOMNode(this.refs.title).value = '';
    findDOMNode(this.refs.description).value = '';
    findDOMNode(this.refs.price).value = '';
    this.setState({img: ''});
  }

  render(){
    const booksList = this.props.books.map(function (book) {
      return (
        <option key={book._id}>{book.title}</option>
      )
    });

    const imgList = this.state.images.map(function (image, index) {
      return (
        <MenuItem
          key={index}
          eventKey={image.name}
          onClick={this.handleSelectImage.bind(this, image.name)}
        >
          {image.name}
        </MenuItem>
      )
    }, this);

    return (
      <Well>
        <Row>
          <Col xs={12} sm={6}>
            <Panel>
              <InputGroup>
                <FormControl type="text" ref="image" value={this.state.img} />
                <DropdownButton
                  componentClass={InputGroup.Button}
                  id="input-dropdown-addon"
                  title="Select an image"
                  bsStyle="primary"
                >
                  { imgList }
                </DropdownButton>
              </InputGroup>
              <Image src={this.state.img} responsive />
            </Panel>
          </Col>
          <Col xs={12} sm={6}>
            <Panel>
              <FormGroup controlId="title" validationState={this.props.validation}>
                <ControlLabel>Title</ControlLabel>
                <FormControl
                  type="text"
                  placeholder="Enter Title"
                  ref="title"/>
                <FormControl.Feedback />
              </FormGroup>
              <FormGroup controlId="description" validationState={this.props.validation}>
                <ControlLabel>Description</ControlLabel>
                <FormControl
                  type="text"
                  placeholder="Enter description"
                  ref="description"/>
                <FormControl.Feedback />
              </FormGroup>
              <FormGroup controlId="price"  validationState={this.props.validation}>
                <ControlLabel>Price</ControlLabel>
                <FormControl
                  type="text"
                  placeholder="Enter price"
                  ref="price"/>
                <FormControl.Feedback />
              </FormGroup>
              <Button
                onClick={(!this.props.msg)?(this.handleSubmit.bind(this)):(this.resetForm.bind(this))}
                bsStyle={(!this.props.style) ? ("primary") : (this.props.style)}
              >
                {(!this.props.msg) ? ("Save book") : (this.props.msg) }
              </Button>
            </Panel>
            <Panel>
              <FormGroup controlId="formControlsSelect">
                <ControlLabel>Select a book id to delete</ControlLabel>
                <FormControl ref="delete" componentClass="select" placeholder="select">
                  {booksList}
                </FormControl>
              </FormGroup>
              <Button
                onClick={this.onDelete.bind(this)}
                bsStyle="danger"
              >
                Delete the book
              </Button>
            </Panel>
          </Col>
        </Row>
      </Well>
    );
  }
}

function mapStateToProps(state){
  return {
    books: state.bookReducers.books,
    msg: state.bookReducers.msg,
    style: state.bookReducers.style,
    validation: state.bookReducers.validation
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    postBooks,
    deleteBook,
    getBooks,
    resetButton
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookForm);
