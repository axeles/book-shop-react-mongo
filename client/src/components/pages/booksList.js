"use strict";

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { getBooks } from "../../actions/bookActions";
import {
  Grid,
  Row,
  Col,
  Carousel
} from 'react-bootstrap';
import BookItem from './bookItem';
import Cart from './cart';

class BooksList extends Component {

  componentDidMount(){
    this.props.getBooks();
  }

  render(){
    const booksList = this.props.books.map(function(book){
      return (
        <Col xs={12} sm={6} md={6} key={book._id}>
          <BookItem
            _id={book._id}
            title={book.title}
            description={book.description}
            image={book.image}
            price={book.price} />
        </Col>
      );
    });
    return (
      <Grid>
        <Row>
          <Carousel>
            <Carousel.Item>
              <img width={900} height={300} alt="900x300" src="img/home1.jpg"/>
              <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img width={900} height={300} alt="900x300" src="img/home2.jpg"/>
              <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </Row>
        <Row>
          <Cart />
        </Row>
        <Row style={{marginTop: '15px'}}>
          {booksList}
        </Row>
      </Grid>
    )
  }
}

function mapStateToProps(state){
  return {
    books: state.bookReducers.books
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getBooks }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BooksList);
