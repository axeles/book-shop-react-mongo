"use strict";

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
  Image,
  Row,
  Col,
  Well,
  Button
} from 'react-bootstrap';
import {addToCart, updateCartItem} from "../../actions/cartActions";


class BookItem extends Component {

  constructor() {
    super();
    this.state = {
      isClicked: false
    }
  }

  onReadMore() {
    this.setState({ isClicked: true });
  }

  onAddToCart(){
    const books = [...this.props.cart, {
      _id: this.props._id,
      title: this.props.title,
      description: this.props.description,
      image: this.props.image,
      price: this.props.price,
      quantity: 1
    }];
    // CHECK IF THE CART IS EMPTY
    if(this.props.cart.length > 0){
      // CART IS NOT EMPTY
      const _id = this.props._id;
      const index = this.props.cart.findIndex(function (item) {
        return item._id === _id;
      });
      if(index === -1){
        // ADD THE BOOKS TO THE CART
        this.props.addToCart(books);
      } else {
        // UPDATE ITEM QUANTITY IN THE CART
        this.props.updateCartItem(_id, this.props.cart[index].quantity + 1, this.props.cart);
      }
    } else {
      // THE CART IS EMPTY. ADD THE BOOKS TO THE CART
      this.props.addToCart(books);
    }
  };

  render(){
    return (
      <Well>
        <Row>
          <Col xs={12} sm={6}>
            <Image className="image" src={this.props.image} />
          </Col>
          <Col xs={6} sm={6}>
            <h4>{this.props.title}</h4>
            <p>{(this.props.description.length > 50 && this.state.isClicked === false)
              ? (this.props.description.substring(0, 50))
              : (this.props.description)}
              <button className="link" onClick={this.onReadMore.bind(this)}>
                {this.state.isClicked === false && this.props.description !== null && this.props.description.length > 50
                ? '...read more' : ''}
              </button>
            </p>
            <h4>$ {this.props.price}</h4>
            <Button onClick={this.onAddToCart.bind(this)} bsStyle='primary'>Add to Cart</Button>
          </Col>
        </Row>
      </Well>
    );
  }
}

function mapStateToProps(state){
  return {
    cart: state.cartReducers.cart
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addToCart, updateCartItem }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookItem);
