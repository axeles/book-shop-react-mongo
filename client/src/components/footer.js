"use strict";

import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="footer text-center">
        <div className="container">
          <p className="footer-text">Copyright &copy; 2017 Book App</p>
        </div>
      </footer>
    );
  }
}

export default Footer;